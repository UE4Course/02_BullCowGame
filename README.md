# Description
Un jeu de devinette simple conçu pour enseigner les rudiments du C ++ et gestion de projet et de solution.

Le jeu est en anglais.

# Objectifs
Découvrir un mot secret caché.
Vous avez un nombre d'essais limité.
A chaque tentative, le jeu indique le nombre de lettres figurant dans le mot secret qui sont bien placées (les "Bulls") et  le nombre de lettres figurant dans le mot secret qui sont mal placées (les "Cows")  

# Mouvements et actions du joueur
Rien de spécial.

# Interactions
Utiliser le clavier pour faire des propositions de mots et répondre aux questions.

# Copyrights
Créé et compilé avec la version 2015 de visual studio et la version 2016 de JetBrain Clion.

Tiré de la formation: "The Unreal Engine Developer Course" de Udemy, section 2.

------------------
(C) 2016 Steamlead (www.steamlead.com)