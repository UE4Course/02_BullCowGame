#pragma once

#include "FBullCowGame.h"
#include <locale>

FBullCowGame::FBullCowGame()
{
	Reset();
}

int32 FBullCowGame::GetMaxTries() const
{
	TMap<int32, int32> WordLengthToMaxTries{
		{3,4},{4,7},{5,10},{6,16},{7,20}
	};
	return WordLengthToMaxTries[MyHiddenWord.length()];
}

int32 FBullCowGame::GetCurrentTry() const
{
	return MyCurrentTry;
}

int32 FBullCowGame::GetHiddenWordLength() const
{
	return MyHiddenWord.length();
}

bool FBullCowGame::IsGameWon() const
{
	return bIsGameWon;
}

void FBullCowGame::Reset()
{
	const FString HIDDEN_WORD = "planet";//this must be an isogram and a word between 3 a 7 letters long (see GetMaxTries)

	MyHiddenWord = HIDDEN_WORD;
	bIsGameWon = false;
	MyCurrentTry = 1;
	return;
}


EGuessStatus FBullCowGame::CheckGuessValidity(FString Word) const
{
	if (Word.length() != GetHiddenWordLength())
	{
		return EGuessStatus::Wrong_Length;
	}
	else if (!IsLowercase(Word))
	{
		return EGuessStatus::Not_Lowercase;
	}
	else if (!IsIsogram(Word))
	{
		return EGuessStatus::Not_Isogram;
	}
	return EGuessStatus::OK;

}

bool FBullCowGame::IsIsogram(FString Word) const
{
	// 0 and 1 letter word are isogram
	if (Word.length() < 2) return true;

	TMap<char, bool> LetterSeen;
	for (char Letter : Word) // For all letter of the Word
	{
		Letter = (char) tolower(Letter);
		if (LetterSeen[Letter]) return false;//Letter already seen, so not isogram
		else LetterSeen[Letter] = true;
	}
	return true;
}

bool FBullCowGame::IsLowercase(FString Word) const
{
	for (auto Letter : Word) // For all the lddetters of the Word
	{
		if (!islower(Letter)) return false;
	}
	return true;
}

FBullCowCount FBullCowGame::SubmitValidGuess(FString Word)
{
	MyCurrentTry++;
	FBullCowCount BullCowCount;

	int32 WordLength = MyHiddenWord.length();
	int32 GuessLength = Word.length();

	for (int32 MHWChar = 0; MHWChar < WordLength; MHWChar++)
	{
		for (int32 GChar = 0; GChar < GuessLength; GChar++)
		{
			if (Word[GChar] == MyHiddenWord[MHWChar])
			{
				if (MHWChar == GChar)
				{
					BullCowCount.Bulls++;
					bIsGameWon = (BullCowCount.Bulls == WordLength);
				}
				else
				{
					BullCowCount.Cows++;
				}
			}
		}
	}
	return BullCowCount;
}
