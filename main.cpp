#pragma once

#include <iostream>
#include <map>
#include "main.h"
#include "FBullCowGame.h"

FBullCowGame BCGame;

int main() {
    do {
        printIntro();
        PlayGame();
    }
    while (AskToPlayAgain());
    return 0;
}

// Plays a signle game
void PlayGame() {
    BCGame.Reset();

    while (!BCGame.IsGameWon() && BCGame.GetCurrentTry() <= BCGame.GetMaxTries()) {
        FBullCowCount BullCowCount = BCGame.SubmitValidGuess(getValidGuess());

        std::cout << "Bulls:" << BullCowCount.Bulls;
        std::cout << ". Cows:" << BullCowCount.Cows << std::endl << std::endl;

        //std::cout << "You guess was:" << Guess << std::endl;
        std::cout << std::endl;
    }

    PrintGameSumary();
    return;
}

bool AskToPlayAgain() {
    std::cout << std::endl << "Do you want to play again ?";
    FText Response = "";
    std::getline(std::cin, Response);
    return Response[0] == 'y' || Response[0] == 'Y';
}

void printIntro() {
    std::cout << "\nWelcome to Bulls & Cows, a fun word game!\n";
    std::cout << "           __(__)__     __n__n__              " << std::endl;
    std::cout << "    .------`-\\00/-'     '-\\00/-`------.      " << std::endl;
    std::cout << "   /   BULL  (oo)         (oo)## COW ##\\     " << std::endl;
    std::cout << "  / \\   __   ./             \\.   __ ##/ \\    " << std::endl;
    std::cout << "     |//   \\|/               \\|/ YY\\\\|      " << std::endl;
    std::cout << "     |||   |||               |||   |||      " << std::endl;

    std::cout << "Can you guess the " << BCGame.GetHiddenWordLength() << " letters isogram word I'm thinking of ?" <<
    std::endl;

}

FText getValidGuess() {
    int32 currentTry = BCGame.GetCurrentTry();
    FText Guess = "";
    EGuessStatus Status;
    do {
        std::cout << "Try " << currentTry << "/" << BCGame.GetMaxTries() << ": Enter the guess:";
        std::getline(std::cin, Guess);
        Status = BCGame.CheckGuessValidity(Guess);
        switch (Status) {
            case EGuessStatus::Not_Lowercase:
                std::cout << "Your word must be lowercase" << std::endl;
                break;
            case EGuessStatus::Wrong_Length:
                std::cout << "Please enter a " << BCGame.GetHiddenWordLength() << " letters Word" << std::endl;
                break;
            case EGuessStatus::Not_Isogram:
                std::cout << "Your word must be an Isogram" << std::endl;
                break;
            case EGuessStatus::OK:
            default:
                // assume the guess is valid
                break;
        }
        std::cout << std::endl;
    }
    while (Status != EGuessStatus::OK);
    return Guess;
}

void PrintGameSumary() {
    if (BCGame.IsGameWon()) {
        std::cout << "Well done !! YOU WIN" << std::endl << std::endl;
    }
    else {
        std::cout << "Bad luck !! YOU LOSE" << std::endl << std::endl;
    }
}
