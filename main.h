#pragma once

// to make syntax unreal friendly
using FText = std::string;

void printIntro();
FText getValidGuess();
void PlayGame();
bool AskToPlayAgain();
void PrintGameSumary();